from flask import Flask, render_template, request
import requests


from flask import Flask
app = Flask(__name__)

persona_api = "https://fakeface.rest/face/json"


@app.route('/')
def index():
    r = requests.get(persona_api).json()
    perso = r['image_url']
    if request.method == 'GET':
        return render_template('index.html', perso=perso)
    else:
        return render_template('index.html', perso=perso)


if __name__ == '__main__':
    app.run()
